/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 91.18518518518519, "KoPercent": 8.814814814814815};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5207407407407407, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.69, 500, 1500, "Get category by id"], "isController": false}, {"data": [0.21666666666666667, 500, 1500, "Create new product"], "isController": false}, {"data": [0.69, 500, 1500, "Login"], "isController": false}, {"data": [0.0, 500, 1500, "Get all products"], "isController": false}, {"data": [0.71, 500, 1500, "Delete product by id"], "isController": false}, {"data": [0.0, 500, 1500, "Get all orders"], "isController": false}, {"data": [0.7266666666666667, 500, 1500, "Create order"], "isController": false}, {"data": [0.7366666666666667, 500, 1500, "Get user information"], "isController": false}, {"data": [0.72, 500, 1500, "Get product rating"], "isController": false}, {"data": [0.03666666666666667, 500, 1500, "Get order by id"], "isController": false}, {"data": [0.14, 500, 1500, "Register"], "isController": false}, {"data": [0.6066666666666667, 500, 1500, "Get product by id"], "isController": false}, {"data": [0.69, 500, 1500, "Create comment of product"], "isController": false}, {"data": [0.5933333333333334, 500, 1500, "Assign product rating"], "isController": false}, {"data": [0.7566666666666667, 500, 1500, "Get comments of product"], "isController": false}, {"data": [0.6466666666666666, 500, 1500, "Get all categories"], "isController": false}, {"data": [0.7333333333333333, 500, 1500, "Get hello message"], "isController": false}, {"data": [0.68, 500, 1500, "Create category"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2700, 238, 8.814814814814815, 12513.234444444432, 19, 61105, 520.0, 46578.20000000002, 60026.0, 60129.979999999996, 8.170503272740477, 28.9130167890603, 3.49678213144978], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Get category by id", 150, 7, 4.666666666666667, 8668.719999999998, 25, 60083, 237.0, 31437.5, 57806.14999999995, 60067.19, 0.5616799472769757, 0.22993772841651192, 0.22642357197554072], "isController": false}, {"data": ["Create new product", 150, 18, 12.0, 18277.680000000004, 54, 60351, 5811.5, 60023.7, 60054.0, 60288.78, 0.5473413804679403, 0.32890727056361563, 0.2859858712944988], "isController": false}, {"data": ["Login", 150, 7, 4.666666666666667, 9186.386666666664, 23, 60231, 147.0, 39397.0, 58549.64999999997, 60184.590000000004, 0.8497187430960352, 0.41984182662622005, 0.28557962678936605], "isController": false}, {"data": ["Get all products", 150, 14, 9.333333333333334, 31085.04666666667, 18020, 60272, 24974.0, 58802.600000000006, 60048.8, 60198.56, 0.5825016504213429, 15.12925923993243, 0.22125961127723195], "isController": false}, {"data": ["Delete product by id", 150, 7, 4.666666666666667, 8517.126666666667, 41, 60180, 260.5, 42188.20000000004, 59709.549999999996, 60162.66, 0.5586550565731354, 0.20165410779063098, 0.26889275382492495], "isController": false}, {"data": ["Get all orders", 150, 13, 8.666666666666666, 33909.69333333333, 14681, 60130, 27420.5, 59312.4, 60044.15, 60128.98, 0.5045612335513038, 0.2539000743807352, 0.19216359115571433], "isController": false}, {"data": ["Create order", 150, 2, 1.3333333333333333, 5256.780000000002, 51, 60158, 297.0, 27831.5, 31002.35, 60087.62, 0.5564809627862631, 0.23221486842935105, 0.2922249639214843], "isController": false}, {"data": ["Get user information", 150, 5, 3.3333333333333335, 8291.326666666664, 19, 60037, 168.5, 33219.50000000001, 58094.6, 60035.47, 0.7542084833370206, 0.3073301365368758, 0.3061516857816617], "isController": false}, {"data": ["Get product rating", 150, 9, 6.0, 9707.426666666668, 28, 61105, 281.0, 34132.8, 60026.0, 60619.48000000001, 0.5584304381817505, 0.21306520722422842, 0.225040922480548], "isController": false}, {"data": ["Get order by id", 150, 9, 6.0, 14201.293333333346, 127, 60219, 5483.0, 36799.0, 60051.0, 60208.8, 0.5407802377269926, 0.26192986553499387, 0.2129639049470576], "isController": false}, {"data": ["Register", 150, 91, 60.666666666666664, 16703.079999999998, 75, 60594, 8890.5, 54504.5, 60118.2, 60412.950000000004, 1.279743368796444, 0.6476534572267109, 0.4451607304348568], "isController": false}, {"data": ["Get product by id", 150, 10, 6.666666666666667, 10601.533333333335, 41, 60211, 419.5, 35679.4, 60027.9, 60161.53, 0.5587070773287842, 0.2859008872268388, 0.21948167347668518], "isController": false}, {"data": ["Create comment of product", 150, 9, 6.0, 8837.153333333335, 38, 60204, 230.0, 30815.8, 60030.05, 60156.57, 0.5597244663027214, 0.27829894020836676, 0.3021200263816799], "isController": false}, {"data": ["Assign product rating", 150, 5, 3.3333333333333335, 6794.640000000002, 115, 60105, 567.0, 30100.100000000002, 56040.24999999998, 60103.98, 0.5580938490616582, 0.25533883621619813, 0.26921851397280966], "isController": false}, {"data": ["Get comments of product", 150, 8, 5.333333333333333, 6020.946666666667, 46, 60134, 231.5, 27883.800000000003, 60020.45, 60084.020000000004, 0.5600546613349463, 5.6705716769623375, 0.22781390130343387], "isController": false}, {"data": ["Get all categories", 150, 11, 7.333333333333333, 10740.413333333334, 72, 60059, 370.0, 34455.6, 60027.9, 60052.37, 0.5564231635253487, 11.575130516008294, 0.2170231465080737], "isController": false}, {"data": ["Get hello message", 150, 6, 4.0, 8314.566666666666, 21, 60079, 150.5, 30308.300000000007, 57502.799999999974, 60066.25, 0.7495839808906056, 0.26393554702140315, 0.2993651023681857], "isController": false}, {"data": ["Create category", 150, 7, 4.666666666666667, 10124.406666666671, 33, 60102, 241.0, 34981.300000000025, 59049.99999999998, 60071.4, 0.5592444979662141, 0.230502668761348, 0.28687859198080673], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 78, 32.773109243697476, 2.888888888888889], "isController": false}, {"data": ["Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 160, 67.22689075630252, 5.925925925925926], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2700, 238, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 160, "400/Bad Request", 78, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["Get category by id", 150, 7, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 7, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Create new product", 150, 18, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 18, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Login", 150, 7, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 7, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get all products", 150, 14, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 14, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Delete product by id", 150, 7, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 7, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get all orders", 150, 13, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 13, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Create order", 150, 2, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get user information", 150, 5, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get product rating", 150, 9, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 9, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get order by id", 150, 9, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 9, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Register", 150, 91, "400/Bad Request", 78, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 13, "", "", "", "", "", ""], "isController": false}, {"data": ["Get product by id", 150, 10, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Create comment of product", 150, 9, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 9, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Assign product rating", 150, 5, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get comments of product", 150, 8, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 8, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get all categories", 150, 11, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 11, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get hello message", 150, 6, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 6, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Create category", 150, 7, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 7, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
